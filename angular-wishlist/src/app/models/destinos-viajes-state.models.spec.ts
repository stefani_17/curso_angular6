import{
    reducerDestinosViajes,
    DestinosViajesState,
    InitMyDataAction,
    InitializeDestinosViajesState,
    NuevoDestinoAction,
} 
from './destinos-viajes-state.models'
import { DestinoViaje } from './destino-viaje.models'

describe('reduerDestinosViajes',()=>{
    it(' should reduce init data', () =>{
        //setup
        const prevState: DestinosViajesState = InitializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1',' destino 2']);
        //action
        const newState: DestinosViajesState = reducerDestinosViajes(prevState,action);
        //assertions
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].n).toEqual('destino 1');
    });
    it(' should reduce new added', () =>{
        const prevState: DestinosViajesState = InitializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona','url'));
        const newState: DestinosViajesState = reducerDestinosViajes(prevState,action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].n).toEqual('barcelona');

    });

})

