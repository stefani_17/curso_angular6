import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, Injectable, APP_INITIALIZER } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Store } from '@ngrx/store';
import Dexie from 'dexie';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { TranslateService, TranslateLoader, TranslateModule } from '@ngx-translate/core'

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { DestinosViajesState,reducerDestinosViajes, initializeDestinosViajesState, DestinosViajesEffects, InitMyDataAction } from './models/destinos-viajes-state.models';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component'
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard'
import { AuthService } from './services/auth.service';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { ReservasReservasListadoComponent } from './reservas-reservas-listado/reservas-reservas-listado.component';
import { DestinoViaje } from './models/destino-viaje.models';
import { Observable, from } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

export interface Appconfig{
  apiEndpoint: string;
}
const APP_CONFIG_VALUE:Appconfig ={
  apiEndpoint: 'http://localhost:3000'
};
export const APP_CONFIG = new InjectionToken<Appconfig>('app.config');

export const childrenRoutesvuelos: Routes =[
  {path:'',redirectTo:'home',pathMatch:'full'},
  {path:'main',component:VuelosMainComponentComponent},
  {path:'mas-info',component:VuelosMasInfoComponentComponent},
  {path: ':id',component: VuelosDetalleComponentComponent},
];
const routes: Routes =[
  {path:'',redirectTo:'home',pathMatch:'full'},
  {path:'home',component:ListaDestinosComponent},
  {path:'destino/id',component:DestinoDetalleComponent},
  {path: 'login',component: LoginComponent},
  {path:'protected',component:ProtectedComponent,canActivate:[UsuarioLogueadoGuard]},
  {path:'vuelos',component:VuelosComponentComponent,canActivate:[UsuarioLogueadoGuard],children:childrenRoutesvuelos}
];
// redux init
export interface AppState{
  destinos:DestinosViajesState;
}
const reducers: ActionReducerMap<AppState>= {
  destinos: reducerDestinosViajes
};
let reducersInitialState ={
  destinos: initializeDestinosViajesState()
};
//redux fin init
export function init_app(appLoadService: appLoadService):() => Promise<any>{
  return () => appLoadService.initializeDestinosViajesState();
}
@Injectable()
class appLoadService{
  constructor(private store: Store<AppState>, private http: HttpClient){}
  async initializeDestinosViajesState(): Promise<any>{
    const headers: HttpHeaders= new HttpHeaders({'X-API-TOKEN':'token-seguridad'});
    const req = new HttpRequest('GET',APP_CONFIG_VALUE.apiEndpoint+'/my',{headers:Headers});
    const response: any= await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}
export class translation{
  constructor(public id:number,public lang:string, public key:string, public value:string){}
}

@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie{
  destinos: Dexie.Table<DestinoViaje,number>;
  translations: Dexie.Table<translation,number>;
  constructor(){
    super('MyDatabase');
    this.version(1).stores({
      destinos: '++id,nombre,imagenurl'
    });
    
    this.version(2).stores({
      destinos: '++id,nombre,imagenurl',
      translations:'++id,key,value'
    });
  }
}
export const db= new MyDatabase();

class TranslationLoader implements TranslateLoader{
  constructor(private http:HttpClient){}
  
  getTranslation(lang:string): Observable<any>{
    const Promise =db.translations
    .where('lang')
    .equals(lang)
    .toArray()
    .then(results => {
      if(results.length ===0){
        return this.http
        .get<translation[]>(APP_CONFIG_VALUE.apiEndpoint+'/api/translation?lang='+lang)
        .toPromise()
        .then(apiResults =>{
          db.translations.bulkAdd(apiResults);
          return apiResults;
        });
      }
      return results;
    }).then((traducciones)=>{
      console.log('traducciones cargadas:');
      console.log(traducciones);
      return traducciones;
    }).then((traducciones)=>{      
        return traducciones.map((t)=> ({[t.key]:t.value}));
    });
    return from(Promise).pipe(flatMap((elems)=> from(elems)));
  }
}
function HttpLoaderFactory(http:HttpClient) {
  return new TranslationLoader(http);  
}
  
@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
    ReservasReservasListadoComponent,
    EspiameDirective,
    TrackearClickDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState}),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    TranslateModule.forRoot({
      loader:{
        provide: TranslateLoader,
        useFactory:(HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [
    AuthService,UsuarioLogueadoGuard,
    {provide: APP_CONFIG,useValue:APP_CONFIG_VALUE},
    appLoadService,
    {provide:APP_INITIALIZER,useFactory:init_app,deps:[appLoadService],multi:true},
    MyDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
